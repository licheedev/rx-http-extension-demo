package rxhttpext.parser;

/**
 * TODO 接口返回数据包裹类，data字段为需要的数据
 */
public class DataWrapper<T> {

    private int code;
    private String msg;
    private T data;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    public void setErrorCode(int errorCode) {
        this.code = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.msg = errorMsg;
    }

    public void setData(T data) {
        this.data = data;
    }
}
