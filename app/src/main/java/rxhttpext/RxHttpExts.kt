package rxhttpext

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import rxhttp.wrapper.exception.HttpStatusCodeException
import rxhttp.wrapper.exception.ParseException
import rxhttp.wrapper.param.RxHttp
import rxhttpext.parser.Bean01Parser
import rxhttpext.parser.BeanParser

/**
 * 获取处理好的http异常信息
 *
 * TODO 如果有特殊格式化需要，可以重改写此方法
 */
val Throwable.httpErrorMsg: String
    get() {
        if (this is ApiException) {
            return this.message!!
        } else if (this is HttpStatusCodeException) {
            return "${defaultHttpErrorMsg}(${httpErrorCode})"
        }
        return this.defaultHttpErrorMsg
    }

/**
 * 获取处理好的http错误码
 *
 * TODO 如果有特殊需要，可以重改写此方法
 */
val Throwable.httpErrorCode: Int
    get() {
        if (this is ApiException) {
            return this.code
        }
        return this.defaultHttpErrorCode
    }


/** 返回实际引起异常的原因 [Throwable.cause] ,如果 [Throwable.cause] 为null，则返回异常本身 */
val Throwable.causeOrMe get() = cause ?: this

/**
 * 接口异常代码
 */
val ParseException.code get() = this.errorCode.toInt()

/** 切换到主线程 */
fun <T> Observable<T>.observeOnMain(): Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}

/** 同步请求 */
@Throws(Throwable::class)
inline fun <reified T> RxHttp<*, *>.executeRawBean(): T {
    return this.execute(BeanParser(T::class.java))
}

/** 同步请求 */
@Throws(Throwable::class)
inline fun <reified T> RxHttp<*, *>.executeBean01(): T {
    return this.execute(Bean01Parser(T::class.java))
} 



