package com.licheedev.rxhttpdemo

import android.app.Application
import com.licheedev.okhttploginterceptor.JsonLoggingInterceptor
import okhttp3.OkHttpClient
import rxhttp.wrapper.cookie.CookieStore
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.ssl.HttpsUtils


class App : Application() {


    override fun onCreate() {
        super.onCreate()

        // 创建okhttp
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(JsonLoggingInterceptor(true,30))
        //val interceptor = HttpLoggingInterceptor()
        //interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        //builder.addInterceptor(interceptor)
        builder.cookieJar(CookieStore())
        //https全通，不安全
        val sslParams = HttpsUtils.getSslSocketFactory();
        builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);
        // 配置rxhttp
        RxHttp.setDebug(false)
        RxHttp.init(builder.build())

    }
}