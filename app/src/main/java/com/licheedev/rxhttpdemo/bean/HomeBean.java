package com.licheedev.rxhttpdemo.bean;

public class HomeBean {

    /**
     * code : 1
     * msg : 获取成功
     * data : {"apk_img":"https://water.dg88888888.com/upload/ueditor/image/20200817/1597624379313483.png","app_code":"https://water.dg88888888.com/index//qrcode?url=h5pro/?macno=0000000003602","wxchat_code":"https://water.dg88888888.com/upload/ueditor/image/20200902/1599026202124064.png","customer":"13066120102","join_name":"吴德闲","join_tel":"13135454542"}
     */

    public int code;
    public String msg;
    public Data data;

    public static class Data {
        /**
         * apk_img : https://water.dg88888888.com/upload/ueditor/image/20200817/1597624379313483.png
         * app_code : https://water.dg88888888.com/index//qrcode?url=h5pro/?macno=0000000003602
         * wxchat_code : https://water.dg88888888.com/upload/ueditor/image/20200902/1599026202124064.png
         * customer : 13066120102
         * join_name : 吴德闲
         * join_tel : 13135454542
         */

        public String apk_img;
        public String app_code;
        public String wxchat_code;
        public String customer;
        public String join_name;
        public String join_tel;
        public String admin_tel;
    }
}
