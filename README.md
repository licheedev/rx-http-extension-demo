# RxHttp自定义解释器Demo


## RxHttp链接
> https://github.com/liujingxing/okhttp-RxHttp

最新依赖参考上面的连接，demo用到的依赖为：
```
// rxhttp依赖
implementation 'com.ljx.rxhttp:rxhttp:2.4.3'
implementation 'com.squareup.okhttp3:okhttp:4.9.0' //rxhttp v2.2.2版本起，需要手动依赖okhttp
kapt 'com.ljx.rxhttp:rxhttp-compiler:2.4.3' //生成RxHttp类，非kotlin项目，请使用annotationProcessor代替kapt
// 协程
implementation 'com.ljx.rxlife:rxlife-coroutine:2.0.1' //管理协程生命周期，页面销毁，关闭请求
//rxjava2   (RxJava2/Rxjava3二选一，使用asXxx方法时必须)
implementation 'io.reactivex.rxjava2:rxjava:2.2.19'
implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'
implementation 'com.ljx.rxlife2:rxlife-rxjava:2.0.0' //管理RxJava2生命周期，页面销毁，关闭请求

// 打log用的
implementation 'com.licheedev:okhttplogginginterceptor:1.0.0'
// rxhttpext文件夹下代码需要的依赖
implementation 'com.licheedev:some-context:1.0.0'
```

扩展解释器代码放在 [<项目>/app/src/main/java/rxhttpext](https://gitee.com/licheedev/rx-http-extension-demo/tree/master/app/src/main/java/rxhttpext)文件夹,使用时建议直接拷贝到项目同样的位置，免得要修改包路径；

初始化
```kotlin
// 创建okhttp
val builder = OkHttpClient.Builder()
builder.addInterceptor(JsonLoggingInterceptor(true,30))
 //https全通，不安全
val sslParams = HttpsUtils.getSslSocketFactory();
builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);
// 配置rxhttp
RxHttp.setDebug(false)
RxHttp.init(builder.build())
```

完整使用例子
```
val macnoParam = mapOf("macno" to "0000000003602")
RxHttp.postForm("apk_index")
    .addAll(macnoParam) // 可以添加一个Map作为请求参数
    .add("time", System.currentTimeMillis()) // 单请求参数
    .apply {
        // 带条件的请求参数
        add("key", if (it == null) "0" else "1")
    }
    .asBean01(HomeBean::class.java)
    .observeOnMain()
    .doFinally {
        LogPlus.i("finished")
    }
    .subscribe({
        LogPlus.i("success=${it}")
    }, {
        LogPlus.e(it.httpErrorMsg)
    })
```

